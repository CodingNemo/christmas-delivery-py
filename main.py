import asyncio
import logging

import click

from delivery_system.elf.domain.elf import NormalElf
from delivery_system.miss_claus.domain.miss_claus import RealMissClaus
from delivery_system.sleigh.domain.sleigh import RealSleigh
from delivery_system.factory.domain.toy_factory import ToyFactory
from delivery_system.factory import infinite_ticker

logging.basicConfig(
    format="%(asctime)s [%(name)s] %(threadName)s:%(thread)d %(levelname)s %(module)s %(message)s",
    level=logging.DEBUG,
)


@click.command()
@click.option("--factories", "-f", default=1, help="number of factories.")
@click.option("--elves", "-e", default=1, help="number of elves.")
def run_pipeline(factories, elves):
    sleigh = RealSleigh()

    miss_claus = RealMissClaus()

    elves = [NormalElf(sleigh) for _ in range(0, elves)]

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([miss_claus.add_elf(elf) for elf in elves]))

    all_factories = [ToyFactory(miss_claus) for j in range(0, factories)]

    return loop.run_until_complete(
        asyncio.wait([factory.produce(infinite_ticker) for factory in all_factories])
    )


if __name__ == "__main__":
    run_pipeline()
