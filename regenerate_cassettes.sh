#!/usr/bin/env bash

/usr/bin/find . -type d -name cassettes -exec rm -rf {} +

NETWORK_NAME=christmas-delivery
NETWORK_ID=`docker network create -d bridge ${NETWORK_NAME}`
echo "NETWORK " ${NETWORK_NAME} " : " ${NETWORK_ID}

MISS_CLAUS=`docker run --name miss_claus --network=${NETWORK_NAME} -d -p 8080:8080 christmas-delivery/miss_claus`
echo "MISS CLAUS " ${MISS_CLAUS}

SLEIGH=`docker run --name sleigh --network=${NETWORK_NAME} -d -p 8081:8081 christmas-delivery/sleigh`
echo "SLEIGH " ${SLEIGH}

ELF=`docker run --name elf --network=${NETWORK_NAME} -d -p 8090:8090 christmas-delivery/elf`
echo "ELF " ${ELF}

echo "RUNNING TESTS WITH CASSETTES"
pipenv run pytest -vv -m with_cassette

echo "KILLING MISS CLAUS"
docker rm -f ${MISS_CLAUS}

echo "KILLING SLEIGH"
docker rm -f ${SLEIGH}

echo "KILLING ELF"
docker rm -f ${ELF}

echo "REMOVING NETWORK"
docker network rm ${NETWORK_ID}

docker ps