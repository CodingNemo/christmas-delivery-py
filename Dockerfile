FROM python:3.7

WORKDIR /usr/src/app

COPY Pipfile .
COPY Pipfile.lock .

RUN pip install pipenv
RUN pipenv sync

COPY delivery_system/__init__.py delivery_system/__init__.py

COPY delivery_system/shared_kernel delivery_system/shared_kernel
COPY delivery_system/tools delivery_system/tools
