#!/usr/bin/env bash

docker build -t christmas-delivery .
docker build -t christmas-delivery/factory delivery_system/factory
docker build -t christmas-delivery/miss_claus delivery_system/miss_claus
docker build -t christmas-delivery/elf delivery_system/elf
docker build -t christmas-delivery/sleigh delivery_system/sleigh