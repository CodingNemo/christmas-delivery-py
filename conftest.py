import asyncio
import http
from functools import wraps

import pytest
import vcr
from aiohttp import (
    ClientConnectorError,
    ClientProxyConnectionError,
    ClientResponseError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from aiohttp.client_exceptions import ConnectionKey
from asynctest import Mock

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.miss_claus import MissClaus
from delivery_system.shared_kernel.present import Present, PresentId
from delivery_system.shared_kernel.sleigh import Sleigh


@pytest.fixture(scope="function")
def mock_miss_claus():
    return Mock(MissClaus, autospec=True)


@pytest.fixture(scope="function")
def mock_sleigh():
    return Mock(Sleigh, autospec=True)


@pytest.fixture(scope="function")
def create_mock_elf():
    def factory():
        return Mock(Elf, autospec=True)

    return factory


@pytest.fixture(scope="function")
def create_mock_elves(create_mock_elf):
    def factory(count=1):
        return [create_mock_elf() for _ in range(0, count)]

    return factory


@pytest.fixture
def cassette(request):
    import os

    path = os.path.dirname(request.module.__file__)
    cassette_file_path = f"{path}/cassettes/{request.node.name}.yaml"
    with vcr.use_cassette(
        cassette_file_path,
        match_on=["method", "scheme", "host", "port", "path", "query", "body"],
    ) as cass:
        yield cass
        if os.path.isfile(cassette_file_path):
            assert cass.all_played


def sample_present() -> Present:
    present_id = PresentId.from_string("87fcf5e9-3f3a-43d2-9c6d-ad4f98fd8619")
    return Present(origin="USA Factory", name="Dog", id=present_id)


def count_attempt(func):
    attempts = {}

    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(inc_attempt(), *args, **kwargs)

    def inc_attempt():
        attempts[func.__name__] = attempts.get(func.__name__, 0) + 1
        return attempts[func.__name__]

    return wrapper


def throw_exception_n_times(n, exception):
    @count_attempt
    def throw_exception(attempt, *args, **kwargs):
        if attempt < n:
            raise exception

    return throw_exception


http_client_exceptions = [
    ClientConnectorError(ConnectionKey, ConnectionError()),
    ClientProxyConnectionError(ConnectionKey, ConnectionError()),
    ClientResponseError(
        request_info=None, history=(), status=http.HTTPStatus.INTERNAL_SERVER_ERROR
    ),
    ServerDisconnectedError(),
    ServerTimeoutError(),
]


class DummyElf(Elf):
    @property
    def behaviour(self) -> str:
        return "dummy"

    @property
    def params(self):
        return {}

    @property
    def name(self):
        return "Dummy"

    async def give(self, present: Present):
        await asyncio.sleep(0)
