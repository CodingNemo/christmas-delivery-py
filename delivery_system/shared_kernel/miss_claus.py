import abc

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.present import Present


class MissClaus(abc.ABC):
    @abc.abstractmethod
    async def dispatch(self, present: Present) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    async def add_elf(self, elf: Elf):
        raise NotImplementedError()
