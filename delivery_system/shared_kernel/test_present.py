from delivery_system.shared_kernel.present import Present, PresentId


def test_present_should_have_an_id_when_created():
    present = Present()
    assert present.id is not None


def test_present_should_create_a_new_instance_of_present_with_a_timestamp():
    original_present = Present()

    from datetime import datetime

    packed_time = datetime.now()
    copy_present = original_present.packed_on(packed_time)

    assert_same_presents_but_different_instances(original_present, copy_present)
    assert original_present.packer_name == copy_present.packer_name
    assert packed_time == original_present.packing_time_stamp


def test_present_should_create_a_new_instance_of_present_with_a_packer():
    original_present = Present()
    packer_name = "John"

    copy_present = original_present.packed_by(packer_name)

    assert_same_presents_but_different_instances(original_present, copy_present)
    assert packer_name == original_present.packer_name
    assert original_present.packing_time_stamp == copy_present.packing_time_stamp


def test_present_with_same_id_should_be_equal():
    present_id_as_string = "ef0518f2-25a7-4d09-a292-2ee73db05cb1"
    present1 = Present(id=PresentId.from_string(present_id_as_string))
    present2 = Present(id=PresentId.from_string(present_id_as_string))
    assert present1 == present2


def test_present_with_different_ids_should_not_be_equal():
    present1 = Present(id=PresentId.from_string("ef0518f2-25a7-4d09-a292-2ee73db05cb2"))
    present2 = Present(id=PresentId.from_string("ef0518f2-25a7-4d09-a292-2ee73db05cb1"))
    assert present1 != present2


def assert_same_presents_but_different_instances(present, present2):
    assert present == present2
    assert id(present) != id(present2)
    assert present.id == present2.id
    assert present.name == present2.name
    assert present.origin == present2.origin
