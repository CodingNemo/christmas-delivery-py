import uuid
from dataclasses import dataclass, field
from datetime import datetime

from delivery_system.tools.name_generator import NameGenerator

present_name_generator = NameGenerator(
    (
        "Xbox One",
        "Playstation 4",
        "Sweater",
        "Nintendo Switch",
        "Train Station",
        "CD Player",
        "TV",
        "Computer",
        "Dog",
        "Cat",
    )
)


@dataclass(frozen=True)
class PresentId:
    value: uuid.UUID

    @classmethod
    def from_string(cls, string: str):
        return cls(uuid.UUID(string))

    def to_string(self):
        return str(self.value)

    @staticmethod
    def new():
        return PresentId(uuid.uuid4())

    def __str__(self) -> str:
        return self.to_string()


@dataclass(eq=False, frozen=True)
class Present:
    name: str = field(default_factory=present_name_generator.new_name)
    id: PresentId = field(default_factory=PresentId.new)

    origin: str = None
    packing_time_stamp: datetime = None
    packer_name: str = None

    @classmethod
    def from_factory(cls, factory_name):
        return cls(origin=factory_name)

    def packed_on(self, time_stamp: datetime):
        present_args = self.__dict__
        present_args["packing_time_stamp"] = time_stamp
        return Present(**present_args)

    def packed_by(self, packer_name: str):
        present_args = self.__dict__
        present_args["packer_name"] = packer_name
        return Present(**present_args)

    def __eq__(self, other):
        return self.id == other.id
