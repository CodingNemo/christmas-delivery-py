import abc

from delivery_system.shared_kernel.present import Present


class Elf(abc.ABC):
    @property
    @abc.abstractmethod
    def behaviour(self) -> str:
        raise NotImplementedError()

    @property
    @abc.abstractmethod
    def params(self):
        raise NotImplementedError()

    @abc.abstractmethod
    async def give(self, present: Present):
        raise NotImplementedError()

    @property
    @abc.abstractmethod
    def name(self) -> str:
        raise NotImplementedError()
