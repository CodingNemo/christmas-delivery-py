import abc

from delivery_system.shared_kernel.present import Present


class Sleigh(abc.ABC):
    @abc.abstractmethod
    async def pack(self, present: Present):
        raise NotImplementedError()
