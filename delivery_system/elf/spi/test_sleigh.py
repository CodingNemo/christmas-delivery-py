import pytest
from asynctest import CoroutineMock

from conftest import sample_present, http_client_exceptions, throw_exception_n_times
from delivery_system.elf import SleighHttpProxy
from delivery_system.tools import LOCALHOST


@pytest.mark.asyncio
@pytest.mark.with_cassette
async def test_should_not_fail_when_packing_into_sleigh(cassette):
    sleigh_url = LOCALHOST.with_port(8081)
    sleigh = SleighHttpProxy(sleigh_url, factor=0, max_tries=1)

    await sleigh.pack(sample_present())


@pytest.mark.asyncio
@pytest.mark.parametrize("exception", http_client_exceptions)
async def test_should_retry_when_packing_fails(exception):
    sleigh_url = LOCALHOST.with_port(8081)

    sleigh = SleighHttpProxy(sleigh_url, base=0, factor=0.5)

    sleigh.packing_through_http = CoroutineMock()
    sleigh.packing_through_http.side_effect = throw_exception_n_times(4, exception)

    await sleigh.pack(sample_present())

    assert 4 == sleigh.packing_through_http.call_count
