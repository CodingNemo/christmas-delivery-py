import pytest
from asynctest import CoroutineMock

from conftest import throw_exception_n_times, http_client_exceptions, DummyElf
from delivery_system.elf import MissClausHttpProxy
from delivery_system.tools import LOCALHOST


@pytest.mark.asyncio
@pytest.mark.parametrize("exception", http_client_exceptions)
async def test_should_retry_when_failing_to_register_to_miss_claus(exception):
    miss_claus_url = LOCALHOST.with_port(8080)
    elf_url = LOCALHOST
    miss_claus = MissClausHttpProxy(miss_claus_url, elf_url, base=0, factor=0.1)

    miss_claus.register_via_http = CoroutineMock()
    miss_claus.register_via_http.side_effect = throw_exception_n_times(4, exception)

    await miss_claus.add_elf(DummyElf())

    miss_claus.register_via_http.assert_called()
    assert 4 == miss_claus.register_via_http.call_count


@pytest.mark.asyncio
@pytest.mark.with_cassette
async def test_should_not_fail_when_registering_to_miss_claus(cassette):
    miss_claus_url = LOCALHOST.with_port(8080)
    elf_url = LOCALHOST
    miss_claus = MissClausHttpProxy(
        miss_claus_url, elf_url, base=1, factor=2, max_value=1
    )

    await miss_claus.add_elf(DummyElf())
