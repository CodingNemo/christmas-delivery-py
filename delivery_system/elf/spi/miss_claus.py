import logging

import aiohttp
from yarl import URL

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.miss_claus import MissClaus
from delivery_system.shared_kernel.present import Present
from delivery_system.tools import expo_backoff_on_http_client_exceptions


class MissClausHttpProxy(MissClaus):
    def __init__(self, miss_claus_url: URL, elf_url: URL, **retry_kwargs) -> None:
        self.retry_args = retry_kwargs
        self.miss_claus_url = miss_claus_url
        self.elf_url = elf_url

    async def dispatch(self, present: Present) -> None:
        raise NotImplementedError()

    async def add_elf(self, elf: Elf):
        @expo_backoff_on_http_client_exceptions(**self.retry_args)
        async def register_with_retry(me):
            return await me.register_via_http(elf)

        await register_with_retry(self)

    async def register_via_http(self, elf):
        register_url = self.miss_claus_url.with_path("/api/register")

        payload = {"elf_url": str(self.elf_url), "elf_name": elf.name}
        logging.debug(f"Registering {payload} to {register_url}")

        async with aiohttp.ClientSession() as session:
            async with session.post(register_url, data=payload) as response:
                response.raise_for_status()
