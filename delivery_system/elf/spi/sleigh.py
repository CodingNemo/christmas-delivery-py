import aiohttp
from yarl import URL

from delivery_system.shared_kernel.present import Present
from delivery_system.shared_kernel.sleigh import Sleigh
from delivery_system.tools import expo_backoff_on_http_client_exceptions


class SleighHttpProxy(Sleigh):
    def __init__(self, url: URL, **retry_kwargs):
        self.url = url
        self.retry_args = retry_kwargs

    async def pack(self, present: Present):
        @expo_backoff_on_http_client_exceptions(**self.retry_args)
        async def pack_with_retry(me):
            return await me.packing_through_http(present)

        await pack_with_retry(self)

    async def packing_through_http(self, present):
        payload = {
            "present_id": present.id.to_string(),
            "present_name": present.name,
            "present_origin": present.origin,
            "present_packer_name": present.packer_name,
        }
        async with aiohttp.ClientSession() as session:
            await session.post(self.url.with_path("/api/pack"), data=payload)
