import asyncio
import logging

from yarl import URL

from delivery_system.elf.domain.elf import NormalElf
from delivery_system.elf.domain.tavern import hire_an_elf
from delivery_system.elf.spi.miss_claus import MissClausHttpProxy
from delivery_system.elf.spi.sleigh import SleighHttpProxy
from delivery_system.shared_kernel.elf import Elf

logging.basicConfig(
    format="%(asctime)s [%(name)s] %(threadName)s:%(thread)d %(levelname)s %(module)s %(message)s",
    level=logging.DEBUG,
)


async def register_to_miss_claus_background_task(miss_claus, elf):
    while True:
        await miss_claus.add_elf(elf)
        await asyncio.sleep(5)


def init_app(app, elf_url: URL, miss_claus_url: URL, sleigh_url: URL, elf_profile):
    sleigh = SleighHttpProxy(sleigh_url)

    elf = hire_an_elf(elf_profile[0], **elf_profile[1])(sleigh)

    miss_claus = MissClausHttpProxy(miss_claus_url, elf_url)
    loop = asyncio.get_event_loop()
    loop.create_task(register_to_miss_claus_background_task(miss_claus, elf))

    app["elf"] = elf


def get_elf(app) -> Elf:
    return app["elf"]
