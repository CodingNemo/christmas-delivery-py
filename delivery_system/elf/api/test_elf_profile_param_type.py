from delivery_system.elf.api import ELF_PROFILE_PARAM_TYPE
from delivery_system.elf.domain.tavern import ElfKind


def test_should_parse_normal_value():
    kind, params = convert_to_elf_profile("normal")

    assert ElfKind.Normal == kind
    assert {} == params


def test_should_parse_lazy_value_without_sleep_time():
    kind, params = convert_to_elf_profile("Lazy")

    assert ElfKind.Lazy == kind
    assert {"sleep_time_s": 1} == params


def test_should_parse_lazy_value_with_sleep_time():
    kind, params = convert_to_elf_profile("lazy:5")

    assert ElfKind.Lazy == kind
    assert params == {"sleep_time_s": 5}


def test_should_parse_lazy_value_with_random_sleep_time():
    kind, params = convert_to_elf_profile("lazy:random")

    assert ElfKind.Lazy == kind
    assert "sleep_time_s" in params

    sleep_time_s = params["sleep_time_s"]
    assert isinstance(sleep_time_s, int)
    assert 2 <= sleep_time_s <= 15


def test_should_parse_lazy_value_with_random_sleep_time_with_upper_bound():
    kind, params = convert_to_elf_profile("lazy:random:7")

    assert ElfKind.Lazy == kind
    assert "sleep_time_s" in params

    sleep_time_s = params["sleep_time_s"]
    assert isinstance(sleep_time_s, int)
    assert 2 <= sleep_time_s <= 7


def test_should_parse_lazy_value_with_random_sleep_time_with_lower_and_upper_bound():
    kind, params = convert_to_elf_profile("lazy:random:3:8")

    assert ElfKind.Lazy == kind
    assert "sleep_time_s" in params

    sleep_time_s = params["sleep_time_s"]
    assert isinstance(sleep_time_s, int)
    assert 3 <= sleep_time_s <= 8


def test_should_parse_grumpy_value_without_threshold():
    kind, params = convert_to_elf_profile("grumpy")
    assert ElfKind.Grumpy == kind
    assert {"threshold": 10} == params


def test_should_parse_grumpy_value_with_threshold():
    kind, params = convert_to_elf_profile("grumpy:20")
    assert ElfKind.Grumpy == kind
    assert {"threshold": 20} == params


def test_should_parse_grumpy_value_with_random_threshold():
    kind, params = convert_to_elf_profile("grumpy:random")

    assert ElfKind.Grumpy == kind
    assert "threshold" in params

    threshold = params["threshold"]
    assert isinstance(threshold, int)
    assert 5 <= threshold <= 20


def test_should_parse_grumpy_value_with_random_threshold_with_upper_bound():
    kind, params = convert_to_elf_profile("grumpy:random:7")

    assert ElfKind.Grumpy == kind
    assert "threshold" in params

    threshold = params["threshold"]
    assert isinstance(threshold, int)
    assert 5 <= threshold <= 7


def test_should_parse_grumpy_value_with_random_threshold_with_lower_and_upper_bound():
    kind, params = convert_to_elf_profile("grumpy:random:3:5")

    assert ElfKind.Grumpy == kind
    assert "threshold" in params

    threshold = params["threshold"]
    assert isinstance(threshold, int)
    assert 3 <= threshold <= 5


def convert_to_elf_profile(value):
    return ELF_PROFILE_PARAM_TYPE.convert(value, None, None)
