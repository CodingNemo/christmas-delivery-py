import click
from yarl.__init__ import URL

from delivery_system.elf.api import ELF_PROFILE_PARAM_TYPE
from delivery_system.elf.api.elf_api import run_http_api
from delivery_system.tools import URL_CLICK_TYPE, LOCALHOST


@click.command()
@click.option("--url", type=URL_CLICK_TYPE, default=LOCALHOST)
@click.option(
    "--miss-claus-url",
    type=URL_CLICK_TYPE,
    help="how to join Miss Claus (URL)",
    prompt=True,
)
@click.option(
    "--sleigh-url",
    type=URL_CLICK_TYPE,
    help="how to fill the Sleigh (URL)",
    prompt=True,
)
@click.option("--profile", type=ELF_PROFILE_PARAM_TYPE, default="normal")
def start_elf(url: URL, miss_claus_url: URL, sleigh_url: URL, profile):
    run_http_api(
        elf_url=url,
        miss_claus_url=miss_claus_url,
        sleigh_url=sleigh_url,
        elf_profile=profile,
    )
