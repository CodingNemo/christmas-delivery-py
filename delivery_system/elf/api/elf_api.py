import asyncio
import http

from aiohttp import web
from aiohttp_swagger import setup_swagger
from yarl import URL

from delivery_system.elf import init_app, get_elf
from delivery_system.shared_kernel.present import Present, PresentId


async def handle_give(request: web.Request) -> web.Response:
    payload = await request.post()

    elf = get_elf(request.app)

    present = Present(
        name=payload["present_name"],
        origin=payload["present_origin"],
        id=PresentId.from_string(payload["present_id"]),
    )

    await elf.give(present)

    return web.Response(status=http.HTTPStatus.CREATED)


async def handle_id(request: web.Request) -> web.Response:
    elf = get_elf(request.app)

    await asyncio.sleep(0)

    profile = {
        "name": elf.name,
        "behaviour": {"type": elf.behaviour, "params": elf.params},
    }

    return web.json_response(status=http.HTTPStatus.OK, data=profile)


async def handle_ping(_: web.Request) -> web.Response:
    await asyncio.sleep(0)
    return web.json_response("Pong")


ping_route = web.get("/api/ping", handle_ping)
give_route = web.post("/api/give", handle_give)
id_route = web.get("/api/id", handle_id)


def create_elf_http_api(
    elf_url: URL, miss_claus_url: URL, sleigh_url: URL, elf_profile
) -> web.Application:
    app = web.Application()
    init_app(
        app,
        elf_url=elf_url,
        miss_claus_url=miss_claus_url,
        sleigh_url=sleigh_url,
        elf_profile=elf_profile,
    )
    app.add_routes([give_route, ping_route, id_route])
    return app


def current_dir():
    import os

    return os.path.dirname(os.path.realpath(__file__))


def run_http_api(
    elf_url: URL, miss_claus_url: URL, sleigh_url: URL, elf_profile
) -> None:
    app = create_elf_http_api(
        elf_url=elf_url,
        miss_claus_url=miss_claus_url,
        sleigh_url=sleigh_url,
        elf_profile=elf_profile,
    )
    setup_swagger(app, swagger_from_file=f"{current_dir()}/elf_api.yml")
    web.run_app(app, port=elf_url.port)
