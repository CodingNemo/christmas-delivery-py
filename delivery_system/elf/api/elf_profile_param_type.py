from random import randint

import click

from delivery_system.elf.domain.tavern import ElfKind


class ElfProfileParamType(click.ParamType):
    name = "profile"

    def convert(self, value, params, ctx):
        kind_as_str, params = value.lower().partition(":")[::2]

        kind = ElfKind(kind_as_str)

        if kind == ElfKind.Lazy:
            return self.convert_to_lazy_profile(params)

        if kind == ElfKind.Grumpy:
            return self.convert_to_rebel_profile(params)

        return kind, {}

    @staticmethod
    def convert_to_lazy_profile(params):
        param, values = params.partition(":")[::2]
        if param == "random":
            lower_bound, upper_bound = ElfProfileParamType.extract_random_bounds(
                values, default_lower_bound=2, default_upper_bound=15
            )
            sleep = randint(lower_bound, upper_bound)
        else:
            sleep = int(param) if param else 1
        return ElfKind.Lazy, {"sleep_time_s": sleep}

    @staticmethod
    def convert_to_rebel_profile(params):
        param, values = params.partition(":")[::2]
        if param == "random":
            lower_bound, upper_bound = ElfProfileParamType.extract_random_bounds(
                values, default_lower_bound=5, default_upper_bound=20
            )
            threshold = randint(lower_bound, upper_bound)
        else:
            threshold = int(param) if param else 10
        return ElfKind.Grumpy, {"threshold": threshold}

    @staticmethod
    def extract_random_bounds(values, default_lower_bound=0, default_upper_bound=10):
        bound1, bound2 = values.partition(":")[::2]
        lower_bound = int(bound1) if bound2 else default_lower_bound
        upper_bound = (
            int(bound2) if bound2 else int(bound1) if bound1 else default_upper_bound
        )
        return lower_bound, upper_bound
