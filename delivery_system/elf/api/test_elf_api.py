import http

import asynctest
import pytest

from delivery_system.elf import get_elf
from delivery_system.elf.api import elf_api
from delivery_system.elf.domain.tavern import ElfKind
from delivery_system.shared_kernel.present import PresentId, Present
from delivery_system.tools import LOCALHOST


@pytest.fixture
def elf_api_cli(loop, aiohttp_client):
    app = elf_api.create_elf_http_api(
        elf_url=LOCALHOST.with_port(8081),
        miss_claus_url=LOCALHOST.with_port(8080),
        sleigh_url=LOCALHOST.with_port(8082),
        elf_profile=(ElfKind.Normal, {}),
    )
    return loop.run_until_complete(aiohttp_client(app))


async def test_handle_give_calls_real_elf_give(elf_api_cli):
    elf = get_elf(elf_api_cli.app)

    present_id = PresentId.new()
    present_name = "nintendo switch"
    present_origin = "North Pole #3"

    payload = {
        "present_name": present_name,
        "present_origin": present_origin,
        "present_id": present_id.to_string(),
    }

    elf.give = asynctest.CoroutineMock()

    response = await elf_api_cli.post("/api/give", data=payload)

    assert 201 == response.status

    elf.give.assert_called_with(
        Present(name=present_name, origin=present_origin, id=present_id)
    )


async def test_handle_id_returns_elf_id(elf_api_cli):
    response = await elf_api_cli.get("/api/id")

    assert http.HTTPStatus.OK == response.status
