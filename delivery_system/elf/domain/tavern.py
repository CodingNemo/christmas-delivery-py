from enum import Enum

from delivery_system.elf import NormalElf
from delivery_system.elf.domain.elf import LazyElf, GrumpyElf


class ElfKind(Enum):
    Normal = "normal"
    Lazy = "lazy"
    Grumpy = "grumpy"


class UnknownElfProfile(Exception):
    pass


def hire_an_elf(kind: ElfKind, **params):
    if kind == ElfKind.Normal:
        return lambda sleigh: NormalElf(sleigh)
    if kind == ElfKind.Lazy:
        return lambda sleigh: LazyElf(sleigh, **params)
    if kind == ElfKind.Grumpy:
        return lambda sleigh: GrumpyElf(sleigh, **params)

    raise UnknownElfProfile()
