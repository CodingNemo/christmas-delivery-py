import logging
import time

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.present import Present
from delivery_system.shared_kernel.sleigh import Sleigh
from delivery_system.tools import as_dict
from delivery_system.tools.name_generator import NameGenerator

elf_name_generator = NameGenerator(
    (
        "Pinecone Iciclecane",
        "Brownie Mullingpears",
        "Licorice Mullingstockings",
        "Licorice Chocolatecrackers",
        "Clove Toffeesnaps",
        "Forest Hollystockings",
        "Nougat Twinklelights",
        "Florentine Tinselsnaps",
        "Pinecone Shinypie",
        "Noel Icetrifle",
        "Peppermint Toffeeballs",
        "Nutmeg Sparklestockings",
        "Cookie Cuddlefig",
        "Gingerbread Emberfig",
        "Clementine Tinsellights",
        "Peaches Twinkleboughs",
    )
)


class NormalElf(Elf):
    @property
    def behaviour(self) -> str:
        return "normal"

    @property
    def params(self):
        return {}

    def __init__(self, sleigh: Sleigh) -> None:
        self.sleigh = sleigh
        self.__name = elf_name_generator.new_name()
        logging.info(f"{type(self).__name__} {self.name} starts working")

    @property
    def name(self):
        return self.__name

    async def give(self, present: Present) -> None:
        logging.info(f"{present.id} : Packing {present.name}")
        await self.sleigh.pack(present.packed_by(self.name))


class LazyElf(NormalElf):
    def __init__(self, sleigh: Sleigh, sleep_time_s: int):
        super().__init__(sleigh)
        logging.debug(f"I'll sleep {sleep_time_s}s first")
        self.sleep_time_s = sleep_time_s

    @property
    def behaviour(self) -> str:
        return "lazy"

    @property
    def params(self):
        return as_dict(sleep_time=f"{self.sleep_time_s}s")

    async def give(self, present: Present) -> None:
        logging.info(
            f"{present.id} : Elf {self.name} is taking a {self.sleep_time_s}s nap."
        )
        time.sleep(self.sleep_time_s)
        await super().give(present)


class NotWorkingAnymore(Exception):
    pass


class GrumpyElf(NormalElf):
    def __init__(self, sleigh: Sleigh, threshold: int) -> None:
        super().__init__(sleigh)
        self.threshold = threshold
        logging.debug(f"I'll need a break every {threshold} presents")
        self.gauge = threshold
        self.working = True

    @property
    def behaviour(self) -> str:
        return "grumpy"

    @property
    def params(self):
        return as_dict(threshold=self.threshold, gauge=self.gauge)

    async def give(self, present: Present) -> None:
        await super().give(present)

        if self.working:
            self.work(present)
        else:
            self.rest(present)
            self.work(present)

    def work(self, present: Present):
        self.gauge -= 1
        if self.gauge < 0:
            logging.info(f"{present.id} : Elf {self.name} stops working.")
            self.working = False
            raise NotWorkingAnymore()
        logging.info(
            f"{present.id} : Elf {self.name} will handle {self.gauge} more presents."
        )

    def rest(self, present: Present):
        logging.info(f"{present.id} : Elf {self.name} is resting {self.threshold}s.")
        time.sleep(self.threshold)
        self.gauge = self.threshold
        self.working = True
