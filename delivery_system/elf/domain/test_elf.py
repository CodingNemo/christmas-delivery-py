from contextlib import asynccontextmanager, contextmanager
from datetime import datetime, timedelta

import pytest

from delivery_system.elf.domain.elf import (
    NormalElf,
    LazyElf,
    GrumpyElf,
    NotWorkingAnymore,
)
from delivery_system.shared_kernel.present import Present


@pytest.mark.asyncio
async def test_normal_elf_give(mock_sleigh):
    present = Present.from_factory("anywhere")
    elf = NormalElf(mock_sleigh)

    await elf.give(present)

    mock_sleigh.pack.assert_called_once_with(present)


def test_normal_elf_behaviour_and_params(mock_sleigh):
    elf = NormalElf(mock_sleigh)

    assert "normal" == elf.behaviour
    assert {} == elf.params


@pytest.mark.asyncio
async def test_lazy_elf_give(mock_sleigh):
    present = Present.from_factory("anywhere")

    elf = LazyElf(mock_sleigh, 2)

    begin = datetime.now()
    await elf.give(present)
    end = datetime.now()

    elapsed = end - begin

    mock_sleigh.pack.assert_called_once_with(present)
    assert elapsed > timedelta(seconds=2)


def test_lazy_elf_behaviour_and_params(mock_sleigh):
    elf = LazyElf(mock_sleigh, 12)

    assert "lazy" == elf.behaviour
    assert {"sleep_time": "12s"} == elf.params


@pytest.mark.asyncio
async def test_grumpy_elf_give_when_threshold_not_reached(mock_sleigh):
    present = Present.from_factory("anywhere")

    elf = GrumpyElf(mock_sleigh, 5)

    async with not_raised(NotWorkingAnymore):
        await elf.give(present)

    assert True == elf.working
    mock_sleigh.pack.assert_called_once_with(present)


@pytest.mark.asyncio
async def test_grumpy_elf_should_stop_working_when_threshold_reached(mock_sleigh):
    present = Present.from_factory("anywhere")

    elf = GrumpyElf(mock_sleigh, 2)

    await elf.give(present)
    await elf.give(present)

    async with raised(NotWorkingAnymore):
        await elf.give(present)

    assert False == elf.working


@pytest.mark.asyncio
async def test_grumpy_elf_should_rest_before_working(mock_sleigh):
    present = Present.from_factory("anywhere")

    elf = GrumpyElf(mock_sleigh, 2)
    elf.gauge = 2
    elf.working = False

    begin = datetime.now()
    await elf.give(present)
    end = datetime.now()

    elapsed = end - begin
    assert elapsed >= timedelta(seconds=2)

    mock_sleigh.pack.assert_called_once_with(present)


def test_grumpy_elf_behaviour_and_params(mock_sleigh):
    elf = GrumpyElf(mock_sleigh, 12)

    assert "grumpy" == elf.behaviour
    assert {"threshold": 12, "gauge": 12} == elf.params


@asynccontextmanager
async def not_raised(exception_type):
    try:
        yield
    except Exception as e:
        if isinstance(e, exception_type):
            pytest.fail(
                f"An Exception  of type {exception_type} occured but it should'nt have."
            )


@asynccontextmanager
async def raised(exception_type):
    exception_was_raised = False
    try:
        yield
    except Exception as e:
        if isinstance(e, exception_type):
            exception_was_raised = True

    if not exception_was_raised:
        pytest.fail(f"Expected to have raise {exception_type} Exception.")
