import pytest

from delivery_system.elf import NormalElf
from delivery_system.elf.domain.elf import LazyElf, GrumpyElf
from delivery_system.elf.domain.tavern import hire_an_elf, ElfKind, UnknownElfProfile


def test_should_hire_a_Normal_elf_when_requested(mock_sleigh):
    elf_profile = hire_an_elf(ElfKind.Normal)
    elf = elf_profile(mock_sleigh)

    assert isinstance(elf, NormalElf)


def test_should_hire_a_Lazy_elf_when_requested(mock_sleigh):
    params = {"sleep_time_s": 10}
    elf_profile = hire_an_elf(ElfKind.Lazy, **params)
    elf = elf_profile(mock_sleigh)

    assert isinstance(elf, LazyElf)
    assert 10 == elf.sleep_time_s


def test_should_hire_a_Rebel_elf_when_requested(mock_sleigh):
    elf_profile = hire_an_elf(ElfKind.Grumpy, threshold=100)
    elf = elf_profile(mock_sleigh)

    assert isinstance(elf, GrumpyElf)
    assert 100 == elf.threshold


def test_should_fail_when_unknown_elf_profile():
    with pytest.raises(UnknownElfProfile):
        hire_an_elf("Some Dude")
