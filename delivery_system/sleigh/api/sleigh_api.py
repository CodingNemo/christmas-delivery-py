import asyncio
import http

from aiohttp import web
from aiohttp_swagger import setup_swagger
from yarl import URL

from delivery_system.shared_kernel.present import Present, PresentId
from delivery_system.sleigh import get_sleigh, init_app
from delivery_system.sleigh.domain.sleigh_stats import SleighStats


async def handle_pack(request):
    payload = await request.post()

    present = Present(
        name=payload["present_name"],
        origin=payload["present_origin"],
        id=PresentId.from_string(payload["present_id"]),
        packer_name=payload["present_packer_name"],
    )

    sleigh = get_sleigh(request.app)

    await sleigh.pack(present)

    return web.Response(status=http.HTTPStatus.CREATED)


async def handle_get_presents(request):
    def dump_present(present: Present):
        return {
            "name": present.name,
            "origin": present.origin,
            "packer": present.packer_name,
            "id": present.id.to_string(),
        }

    sleigh = get_sleigh(request.app)

    body = {"count": len(sleigh), "presents": [dump_present(p) for p in sleigh]}

    return web.json_response(data=body)


async def handle_stats_debit(request):
    sleigh = get_sleigh(request.app)
    stats = SleighStats(sleigh)

    body = {"value": stats.debit.value, "unit": stats.debit.unit}

    return web.json_response(data=body)


async def handle_stats_packers(request):
    sleigh = get_sleigh(request.app)
    stats = SleighStats(sleigh)

    return web.json_response(data=stats.packed_by)


async def handle_ping(_: web.Request) -> web.Response:
    await asyncio.sleep(0)
    return web.json_response("Pong")


ping_route = web.get("/api/ping", handle_ping)

pack_route = web.post("/api/pack", handle_pack)
presents_route = web.get("/api/presents", handle_get_presents)

stats_debit_route = web.get("/api/stats/debit", handle_stats_debit)
stats_packers_route = web.get("/api/stats/packed_by", handle_stats_packers)


def create_sleigh_http_api():
    app = web.Application()
    init_app(app)
    app.add_routes(
        [pack_route, presents_route, ping_route, stats_debit_route, stats_packers_route]
    )
    return app


def current_dir():
    import os

    return os.path.dirname(os.path.realpath(__file__))


def run_http_api(url: URL):
    app = create_sleigh_http_api()
    setup_swagger(app, swagger_from_file=f"{current_dir()}/sleigh_api.yml")
    web.run_app(app, port=url.port)
