import pytest

from delivery_system.shared_kernel.present import PresentId, Present
from delivery_system.sleigh import get_sleigh
from delivery_system.sleigh.api.sleigh_api import create_sleigh_http_api


@pytest.fixture
def sleigh_api_cli(loop, aiohttp_client):
    app = create_sleigh_http_api()
    return loop.run_until_complete(aiohttp_client(app))


async def test_handle_pack_adds_a_present_to_the_sleigh(sleigh_api_cli):
    sleigh = get_sleigh(sleigh_api_cli.app)

    present_id = PresentId.new()
    present_name = "nintendo switch"
    present_origin = "North Pole #3"
    present_packer_name = "Marcus"
    payload = {
        "present_name": present_name,
        "present_origin": present_origin,
        "present_id": present_id.to_string(),
        "present_packer_name": present_packer_name,
    }

    response = await sleigh_api_cli.post("/api/pack", data=payload)

    assert 201 == response.status

    assert Present(id=present_id) in sleigh


async def test_handle_get_returns_presents_packed_in_the_sleigh(sleigh_api_cli):
    sleigh = get_sleigh(sleigh_api_cli.app)
    sleigh.reset()

    present1 = Present()
    present2 = Present()

    await sleigh.pack(present1)
    await sleigh.pack(present2)

    response = await sleigh_api_cli.get("/api/presents")

    assert 200 == response.status

    body = await response.json()

    assert "count" in body
    assert 2 == body["count"]

    presents_in_body = [
        Present(id=PresentId.from_string(p["id"])) for p in body["presents"]
    ]

    assert present1 in presents_in_body
    assert present2 in presents_in_body
