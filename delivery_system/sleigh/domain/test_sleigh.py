import pytest

from delivery_system.shared_kernel.present import Present
from delivery_system.sleigh.domain.sleigh import RealSleigh


@pytest.mark.asyncio
async def test_pack():
    sleigh = RealSleigh()
    present = Present("factory", "name")
    await sleigh.pack(present)
    assert present in sleigh
