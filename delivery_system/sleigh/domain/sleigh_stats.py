from collections import defaultdict
from dataclasses import dataclass

from delivery_system.sleigh import RealSleigh


class SleighStats:
    def __init__(self, sleigh: RealSleigh) -> None:
        self.sleigh = sleigh
        self.debit = self.__calculate_debit(sleigh)
        self.packed_by = self.__group_by_packers(sleigh)

    def __calculate_debit(self, sleigh):
        unit = "presents/s"
        total_presents = len(sleigh)

        if total_presents == 0:
            return Debit(0, unit)

        if total_presents == 1:
            return Debit(1, unit)

        elapsed_time_delta = self.__elapsed_time_delta(sleigh)

        debit_value = total_presents / elapsed_time_delta.seconds
        return Debit(debit_value, unit)

    @staticmethod
    def __elapsed_time_delta(sleigh):
        sorted_presents = sorted(sleigh, key=lambda p: p.packing_time_stamp)
        first_present = sorted_presents[0]
        last_present = sorted_presents[-1]
        elapsed_time_delta = (
            last_present.packing_time_stamp - first_present.packing_time_stamp
        )
        return elapsed_time_delta

    @staticmethod
    def __group_by_packers(sleigh):
        packers_stats = defaultdict(int)

        for present in sleigh:
            packers_stats[present.packer_name] += 1

        return {packer: count for (packer, count) in packers_stats.items()}


@dataclass(frozen=True)
class Debit:
    value: float
    unit: str
