from datetime import datetime, timedelta

import pytest

from delivery_system.shared_kernel.present import Present
from delivery_system.sleigh import RealSleigh
from delivery_system.sleigh.domain.sleigh_stats import SleighStats, Debit


@pytest.mark.asyncio
async def test_should_extract_packing_debit_from_sleigh():
    sleigh = real_sleigh()

    await sleigh.pack(Present())
    await sleigh.pack(Present())

    sleigh_stats = SleighStats(sleigh)

    assert Debit(2.0, "presents/s") == sleigh_stats.debit


@pytest.mark.asyncio
async def test_should_extract_packing_debit_from_sleigh_when_only_one_packed_present():
    sleigh = real_sleigh()

    await sleigh.pack(Present())

    sleigh_stats = SleighStats(sleigh)

    assert Debit(1.0, "presents/s") == sleigh_stats.debit


def test_should_return_a_zero_debit_when_no_packed_presents_in_sleigh():
    sleigh = RealSleigh()

    sleigh_stats = SleighStats(sleigh)

    assert Debit(0.0, "presents/s") == sleigh_stats.debit


@pytest.mark.asyncio
async def test_should_count_the_number_of_presents_packed_per_elf():
    factory_name = "Factory"

    sleigh = real_sleigh()

    await sleigh.pack(Present.from_factory(factory_name).packed_by("John"))
    await sleigh.pack(Present.from_factory(factory_name).packed_by("John"))
    await sleigh.pack(Present.from_factory(factory_name).packed_by("John"))

    await sleigh.pack(Present.from_factory(factory_name).packed_by("Jane"))
    await sleigh.pack(Present.from_factory(factory_name).packed_by("Jane"))

    await sleigh.pack(Present.from_factory(factory_name).packed_by("Jimmy"))

    sleigh_stats = SleighStats(sleigh)

    assert {"John": 3, "Jane": 2, "Jimmy": 1} == sleigh_stats.packed_by


def real_sleigh():
    one_date_per_second_generator = now_date_generator(timedelta(seconds=1))
    sleigh = RealSleigh(lambda: next(one_date_per_second_generator))
    return sleigh


def now_date_generator(frequency):
    current = datetime.now()
    while True:
        current = current + frequency
        yield current
