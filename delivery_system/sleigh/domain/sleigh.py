import asyncio
import logging
from datetime import datetime
from typing import List, Callable

from delivery_system.shared_kernel.present import Present
from delivery_system.shared_kernel.sleigh import Sleigh


class RealSleigh(Sleigh):
    def __init__(self, now: Callable[[], datetime] = datetime.now) -> None:
        self.__now = now
        self.__presents: List[Present] = []

    def __len__(self):
        return len(self.__presents)

    def __iter__(self):
        return self.__presents.__iter__()

    def reset(self):
        self.__presents = []

    async def pack(self, present: Present):
        self.__presents.append(present.packed_on(self.__now()))
        logging.info(f"{present.id} Packed {present.name}")
        await asyncio.sleep(0)
