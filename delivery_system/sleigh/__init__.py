import logging

from delivery_system.sleigh.domain.sleigh import RealSleigh

logging.basicConfig(
    format="%(asctime)s [%(name)s] %(threadName)s:%(thread)d %(levelname)s %(module)s %(message)s",
    level=logging.DEBUG,
)


def init_app(app):
    app["sleigh"] = RealSleigh()


def get_sleigh(app):
    return app["sleigh"]
