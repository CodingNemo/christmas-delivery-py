import click
from yarl import URL

from delivery_system.sleigh.api.sleigh_api import run_http_api
from delivery_system.tools import LOCALHOST
from delivery_system.tools import URL_CLICK_TYPE


@click.command()
@click.option("--url", type=URL_CLICK_TYPE, default=LOCALHOST)
def run_sleigh(url: URL):
    run_http_api(url)


if __name__ == "__main__":
    run_sleigh()
