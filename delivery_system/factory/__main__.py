import asyncio
import time

import click
from yarl import URL

from delivery_system.factory import MissClausHttpProxy, ToyFactory, infinite_ticker
from delivery_system.tools import URL_CLICK_TYPE


@click.command()
@click.option("--name", default=None, help="name of the factory")
@click.option("--delay", type=float, default=1, prompt=True)
@click.option(
    "--miss-claus-url",
    type=URL_CLICK_TYPE,
    help="how to join Miss Claus (URL)",
    prompt=True,
)
def run_factory(name: str, delay: float, miss_claus_url: URL):
    async def async_run_factory():
        miss_claus = MissClausHttpProxy(miss_claus_url)
        factory = ToyFactory(miss_claus, name)
        await factory.produce(infinite_ticker(delay))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([async_run_factory()]))


if __name__ == "__main__":
    run_factory()
