import logging
import time

from delivery_system.factory.domain.toy_factory import ToyFactory
from delivery_system.factory.spi.miss_claus import MissClausHttpProxy

logging.basicConfig(
    format="%(asctime)s [%(name)s] %(threadName)s:%(thread)d %(levelname)s %(module)s %(message)s",
    level=logging.DEBUG,
)


def infinite_ticker(sleep_time_seconds: float):
    def wrapper():
        while True:
            time.sleep(sleep_time_seconds)
            yield

    return wrapper
