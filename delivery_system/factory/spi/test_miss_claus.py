import pytest
from asynctest import CoroutineMock

from conftest import sample_present, http_client_exceptions, throw_exception_n_times
from delivery_system.factory import MissClausHttpProxy
from delivery_system.tools import LOCALHOST


@pytest.mark.asyncio
@pytest.mark.with_cassette
async def test_dispatch_package_to_miss_claus_should_not_fail(cassette):
    miss_claus_url = LOCALHOST.with_port(8080)
    miss_claus = MissClausHttpProxy(miss_claus_url)
    present = sample_present()

    await miss_claus.dispatch(present)


@pytest.mark.asyncio
@pytest.mark.parametrize("exception", http_client_exceptions)
async def test_dispatch_package_to_miss_claus_should_not_fail(exception):
    miss_claus_url = LOCALHOST.with_port(8080)
    miss_claus = MissClausHttpProxy(miss_claus_url, base=0, factor=0.5)

    miss_claus.dispatch_through_http = CoroutineMock()
    miss_claus.dispatch_through_http.side_effect = throw_exception_n_times(4, exception)

    present = sample_present()
    await miss_claus.dispatch(present)

    assert 4 == miss_claus.dispatch_through_http.call_count
