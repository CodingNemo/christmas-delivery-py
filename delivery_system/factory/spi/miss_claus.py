import logging

import aiohttp
from yarl import URL

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.miss_claus import MissClaus
from delivery_system.shared_kernel.present import Present
from delivery_system.tools import expo_backoff_on_http_client_exceptions


class MissClausHttpProxy(MissClaus):
    def __init__(self, url: URL, **retry_kwargs) -> None:
        self.retry_args = retry_kwargs
        self.url = url

    async def dispatch(self, present: Present) -> None:
        @expo_backoff_on_http_client_exceptions(**self.retry_args)
        async def dispatch_with_retry(me):
            return await me.dispatch_through_http(present)

        return await dispatch_with_retry(self)

    async def dispatch_through_http(self, present):
        payload = {
            "present_id": present.id.to_string(),
            "present_name": present.name,
            "present_origin": present.origin,
        }
        async with aiohttp.ClientSession() as session:
            dispatch_url = self.url.with_path("/api/dispatch")
            logging.debug(f"{present.id} : Dispatching {payload} to {dispatch_url}")
            response = await session.post(dispatch_url, data=payload)
            return response.status

    async def add_elf(self, elf: Elf):
        raise NotImplementedError()
