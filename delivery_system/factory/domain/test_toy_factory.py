import pytest

from delivery_system.factory.domain.toy_factory import ToyFactory


def one_tick_ticker():
    yield


def many_tick_ticker(count):
    for i in range(0, count):
        yield


@pytest.mark.asyncio
async def test_produce_one_present_every_tick(mock_miss_claus):
    factory = ToyFactory(mock_miss_claus)

    await factory.produce(one_tick_ticker)

    mock_miss_claus.dispatch.assert_called_once()


@pytest.mark.asyncio
async def test_produce_one_present_every_tick_when_many_ticks(mock_miss_claus):
    factory = ToyFactory(mock_miss_claus)

    await factory.produce(lambda: many_tick_ticker(3))
    assert 3 == mock_miss_claus.dispatch.call_count
