import logging

from delivery_system.shared_kernel.miss_claus import MissClaus
from delivery_system.shared_kernel.present import Present
from delivery_system.tools.name_generator import NameGenerator

toy_factory_name_generator = NameGenerator(("North Pole Factory",))


class ToyFactory:
    def __init__(self, miss_claus: MissClaus, name: str = None) -> None:
        self.miss_claus = miss_claus
        self.__name = name or toy_factory_name_generator.new_name()
        logging.info(f"New Toy Factory '{self.__name}'")

    async def produce(self, ticker) -> None:
        logging.debug(f"START PRODUCING")
        for _ in ticker():
            present = Present.from_factory(self.__name)
            logging.info(f"{present.id} : Producing {present.name}")
            await self.miss_claus.dispatch(present)
