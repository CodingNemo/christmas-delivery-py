import logging

from delivery_system.miss_claus.domain.miss_claus import RealMissClaus

logging.basicConfig(
    format="%(asctime)s [%(name)s] %(threadName)s:%(thread)d %(levelname)s %(module)s %(message)s",
    level=logging.DEBUG,
)


def init_app(app):
    app["miss_claus"] = RealMissClaus()


def get_miss_claus(app):
    return app["miss_claus"]
