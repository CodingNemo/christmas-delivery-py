import asyncio
import logging

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.miss_claus import MissClaus
from delivery_system.shared_kernel.present import Present


class RealMissClaus(MissClaus):
    def __init__(self) -> None:
        self.available_elves = []
        self.busy_elves = []
        self.elf_picking_sync = asyncio.Lock()

    async def dispatch(self, present: Present) -> None:
        available_elf = await self.pick_an_elf()
        logging.info(f"{present.id} : Dispatching to {present.name}")
        try:
            await available_elf.give(present)
            self.release(available_elf)
        except Exception as e:
            self.discard_elf(available_elf, e)
            await self.dispatch(present)

    async def add_elf(self, elf: Elf):
        await asyncio.sleep(0)
        added = elf not in self.available_elves
        if added:
            logging.debug(f"Adding {elf.name}")
            self.available_elves.append(elf)
        return added

    async def pick_an_elf(self):
        async with self.elf_picking_sync:
            if not self.available_elves:
                logging.info(f"Waiting for an available elf")
                await self.for_an_elf_to_be_available()

            available_elf = self.first_available_elf()
            self.available_elves.remove(available_elf)
            self.busy_elves.append(available_elf)
            return available_elf

    def release(self, elf: Elf):
        logging.debug(f"Releasing {elf.name}")
        self.busy_elves.remove(elf)
        self.available_elves.append(elf)

    def discard_elf(self, elf: Elf, reason):
        logging.debug(f"Discarding {elf.name} : {reason}")
        self.busy_elves.remove(elf)

    def first_available_elf(self) -> Elf:
        logging.debug(f"{len(self.available_elves)} available elves")
        logging.debug(f"{len(self.busy_elves)} busy elves")
        return self.available_elves[0]

    async def for_an_elf_to_be_available(self):
        while not self.available_elves:
            await asyncio.sleep(1)
