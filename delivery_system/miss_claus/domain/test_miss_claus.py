import asyncio

import pytest

from delivery_system.elf.domain.elf import LazyElf
from delivery_system.miss_claus.domain.miss_claus import RealMissClaus
from delivery_system.shared_kernel.present import Present
from delivery_system.shared_kernel.sleigh import Sleigh


class MockedLazyElf(LazyElf):
    def __init__(self, sleigh: Sleigh, sleep_time_s: int):
        super().__init__(sleigh, sleep_time_s)
        self.packed_presents = []

    async def give(self, present: Present) -> None:
        await super().give(present)
        self.packed_presents.append(present)


@pytest.mark.asyncio
async def test_miss_claus_dispatches_present_to_first_available_elf(mock_sleigh):
    presents = some_presents(1)

    elf = MockedLazyElf(mock_sleigh, 0)

    await dispatch_all_presents([elf], presents)

    assert_has_packed(elf, presents[0])


@pytest.mark.asyncio
async def test_miss_claus_dispatches_presents_to_all_available_elves(mock_sleigh):
    presents = some_presents(4)

    elf1, elf2, elf3 = (
        MockedLazyElf(mock_sleigh, 0),
        MockedLazyElf(mock_sleigh, 1),
        MockedLazyElf(mock_sleigh, 2),
    )

    await dispatch_all_presents([elf1, elf2, elf3], presents)

    assert_has_packed(elf1, number_of_presents=2)
    assert_has_packed(elf2, number_of_presents=1)
    assert_has_packed(elf3, number_of_presents=1)


@pytest.mark.asyncio
async def test_miss_claus_should_wait_for_an_available_elf(mock_sleigh):
    presents = some_presents(2)

    elf = MockedLazyElf(mock_sleigh, 0)

    await dispatch_all_presents([elf], presents)

    assert_has_packed(elf, number_of_presents=len(presents))


class SomeException(Exception):
    pass


@pytest.mark.asyncio
async def test_miss_claus_should_discard_an_elf_if_it_fails_to_pack_a_present(
    mock_sleigh, create_mock_elf
):
    presents = some_presents(1)

    failing_elf = create_mock_elf()
    failing_elf.give.side_effect = SomeException

    some_other_elf = MockedLazyElf(mock_sleigh, 0)

    miss_claus = await dispatch_all_presents([failing_elf, some_other_elf], presents)

    assert_has_packed(some_other_elf, presents[0])

    assert 1 == len(miss_claus.available_elves)
    assert 0 == len(miss_claus.busy_elves)


@pytest.mark.asyncio
async def test_miss_claus_should_add_an_elf(create_mock_elf):
    elf = create_mock_elf()
    miss_claus = RealMissClaus()

    added = await miss_claus.add_elf(elf)

    assert 1 == len(miss_claus.available_elves)
    assert added


@pytest.mark.asyncio
async def test_miss_claus_should_not_add_the_same_elf_twice(create_mock_elf):
    elf = create_mock_elf()
    miss_claus = RealMissClaus()

    await miss_claus.add_elf(elf)

    added = await miss_claus.add_elf(elf)

    assert 1 == len(miss_claus.available_elves)
    assert not added


def assert_has_packed(elf, present=None, number_of_presents=1):
    if present:
        assert present in elf.packed_presents
    else:
        assert number_of_presents == len(elf.packed_presents)


async def dispatch_all_presents(elves, presents):
    miss_claus = RealMissClaus()
    for elf in elves:
        await miss_claus.add_elf(elf)
    await asyncio.wait([miss_claus.dispatch(present) for present in presents])
    return miss_claus


def some_presents(number=2):
    return [Present.from_factory("Factory") for _ in range(0, number)]
