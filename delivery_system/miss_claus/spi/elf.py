import logging

import aiohttp
from yarl import URL

from delivery_system.shared_kernel.elf import Elf
from delivery_system.shared_kernel.present import Present


class ElfHttpProxy(Elf):
    @property
    def params(self):
        return _build_params(url=self.url)

    @property
    def behaviour(self) -> str:
        return "proxy"

    def __init__(self, url: URL, name: str) -> None:
        self.url = url
        self.__name = name

    async def give(self, present: Present):
        give_url = self.url.with_path("/api/give")
        payload = {
            "present_id": present.id.to_string(),
            "present_name": present.name,
            "present_origin": present.origin,
        }
        logging.debug(f"{present.id} Give present {present.name} to {give_url}")
        async with aiohttp.ClientSession() as session:
            await session.post(give_url, data=payload)

    @property
    def name(self) -> str:
        return self.__name

    def __str__(self) -> str:
        return f"{self.name}@{self.url}"

    def __eq__(self, o: object) -> bool:
        return isinstance(o, ElfHttpProxy) and self.name == o.name and self.url == o.url
