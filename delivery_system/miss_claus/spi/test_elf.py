import pytest

from conftest import sample_present
from delivery_system.miss_claus.spi.elf import ElfHttpProxy
from delivery_system.tools import LOCALHOST


@pytest.mark.asyncio
@pytest.mark.with_cassette
async def test_should_not_fail_when_giving_a_present_to_an_elf(cassette):
    elf_url = LOCALHOST.with_port(8090)
    elf = ElfHttpProxy(elf_url, "Johnny")
    present = sample_present()
    print(f" - elf {elf}")
    print(f" - present {present}")

    await elf.give(present)
