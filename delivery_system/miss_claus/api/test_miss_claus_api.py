import http

import asynctest
import pytest

from delivery_system.miss_claus import init_app, get_miss_claus
from delivery_system.shared_kernel.present import PresentId, Present


@pytest.fixture
def miss_claus_api_cli(loop, aiohttp_client):
    from delivery_system.miss_claus.api import miss_claus_api

    app = miss_claus_api.create_miss_claus_http_api_app()
    init_app(app)
    return loop.run_until_complete(aiohttp_client(app))


async def test_handle_dispatch_calls_real_miss_claus_dispatch(miss_claus_api_cli):
    present_id = PresentId.new()
    present_name = "nintendo switch"
    present_origin = "North Pole #3"

    payload = {
        "present_name": present_name,
        "present_origin": present_origin,
        "present_id": present_id.to_string(),
    }

    miss_claus = get_miss_claus(miss_claus_api_cli.app)
    miss_claus.dispatch = asynctest.CoroutineMock()

    response = await miss_claus_api_cli.post("/api/dispatch", data=payload)

    assert 201 == response.status

    miss_claus.dispatch.assert_called_with(
        Present(name=present_name, origin=present_origin, id=present_id)
    )


async def test_register_adds_a_new_elf_to_miss_claus(miss_claus_api_cli):
    payload = {"elf_url": "http://0.0.0.0:2000", "elf_name": "Jimmy"}

    miss_claus = get_miss_claus(miss_claus_api_cli.app)
    miss_claus.dispatch = asynctest.CoroutineMock()

    miss_claus.add_elf = asynctest.CoroutineMock()
    miss_claus.add_elf.return_value = True

    response = await miss_claus_api_cli.post("/api/register", data=payload)

    assert 201 == response.status

    miss_claus.add_elf.assert_called_once()


async def test_register_many_times_the_same_elf(miss_claus_api_cli):
    payload = {"elf_url": "http://0.0.0.0:2000", "elf_name": "Jimmy"}

    miss_claus = get_miss_claus(miss_claus_api_cli.app)
    miss_claus.dispatch = asynctest.CoroutineMock()

    miss_claus.add_elf = asynctest.CoroutineMock()
    miss_claus.add_elf.return_value = False

    response = await miss_claus_api_cli.post("/api/register", data=payload)

    assert http.HTTPStatus.OK == response.status
