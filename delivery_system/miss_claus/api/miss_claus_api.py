import asyncio
import http

from aiohttp import web
from aiohttp_swagger import setup_swagger
from yarl import URL

from delivery_system.miss_claus import init_app, get_miss_claus
from delivery_system.miss_claus.spi.elf import ElfHttpProxy
from delivery_system.shared_kernel.present import Present, PresentId


async def handle_dispatch(request: web.Request):
    payload = await request.post()

    present = Present(
        name=payload["present_name"],
        origin=payload["present_origin"],
        id=PresentId.from_string(payload["present_id"]),
    )

    miss_claus = get_miss_claus(request.app)

    await miss_claus.dispatch(present)

    return web.json_response(status=http.HTTPStatus.CREATED)


async def handle_get_elves(request: web.Request):
    miss_claus = get_miss_claus(request.app)

    elves = miss_claus.available_elves()

    return web.json_response(data=elves)


async def handle_register(request: web.Request):
    payload = await request.post()

    elf_url = payload["elf_url"]
    elf_name = payload["elf_name"]

    elf = ElfHttpProxy(URL(elf_url), elf_name)

    miss_claus = get_miss_claus(request.app)
    added = await miss_claus.add_elf(elf)

    if added:
        return web.json_response(status=http.HTTPStatus.CREATED)

    return web.json_response(status=http.HTTPStatus.OK)


async def handle_ping(_: web.Request) -> web.Response:
    await asyncio.sleep(0)
    return web.json_response("Pong")


ping_route = web.get("/api/ping", handle_ping)

dispatch_route = web.post("/api/dispatch", handle_dispatch)
register_route = web.post("/api/register", handle_register)
get_elves_route = web.get("/api/elves", handle_get_elves)


def create_miss_claus_http_api_app():
    app = web.Application()
    init_app(app)
    app.add_routes([dispatch_route, register_route, get_elves_route, ping_route])
    return app


def current_dir():
    import os

    return os.path.dirname(os.path.realpath(__file__))


def run_http_api(url: URL):
    app = create_miss_claus_http_api_app()
    setup_swagger(app, swagger_from_file=f"{current_dir()}/miss_claus_api.yml")
    web.run_app(app, port=url.port)
