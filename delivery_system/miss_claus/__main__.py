import click
from yarl import URL

from delivery_system.miss_claus.api.miss_claus_api import run_http_api
from delivery_system.tools import LOCALHOST, URL_CLICK_TYPE


@click.command()
@click.option("--url", type=URL_CLICK_TYPE, default=LOCALHOST)
def run_miss_claus(url: URL):
    run_http_api(url)


if __name__ == "__main__":
    run_miss_claus()
