import asyncio

import pytest

from delivery_system.elf.domain.elf import NormalElf
from delivery_system.miss_claus.domain.miss_claus import RealMissClaus
from delivery_system.sleigh.domain.sleigh import RealSleigh
from delivery_system.factory.domain.test_toy_factory import many_tick_ticker
from delivery_system.factory.domain.toy_factory import ToyFactory


@pytest.mark.asyncio
async def test_delivery_pipeline_with_one_factory_and_one_elf():
    sleigh = RealSleigh()

    miss_claus = RealMissClaus()
    elf = NormalElf(sleigh)

    await miss_claus.add_elf(elf)

    factory = ToyFactory(miss_claus)

    await factory.produce(lambda: many_tick_ticker(2))

    assert 2 == len(sleigh)


@pytest.mark.asyncio
async def test_delivery_pipeline_with_many_factory_and_many_elf():
    sleigh = RealSleigh()
    miss_claus = RealMissClaus()

    elf1 = NormalElf(sleigh)
    await miss_claus.add_elf(elf1)

    elf2 = NormalElf(sleigh)
    await miss_claus.add_elf(elf2)

    factory1 = ToyFactory(miss_claus)
    factory2 = ToyFactory(miss_claus)
    factory3 = ToyFactory(miss_claus)

    await asyncio.wait(
        [
            factory1.produce(lambda: many_tick_ticker(3)),
            factory2.produce(lambda: many_tick_ticker(2)),
            factory3.produce(lambda: many_tick_ticker(1)),
        ]
    )

    assert 6 == len(sleigh)
