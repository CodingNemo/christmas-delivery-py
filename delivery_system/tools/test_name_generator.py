from delivery_system.tools.name_generator import NameGenerator


def test_should_generate_a_new_name_picked_in_a_list_of_names():
    name_generator = NameGenerator(("name1", "name2"))

    new_name = name_generator.new_name()

    assert new_name is not None
    assert new_name in ["name1", "name2"]


def test_should_generate_a_new_name_with_a_number_when_picked_twice():
    name_generator = NameGenerator(("name1",))

    name_generator.new_name()
    new_name = name_generator.new_name()

    assert "name1 #1" == new_name
