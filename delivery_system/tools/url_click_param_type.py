import click
from yarl import URL


class UrlClickParamType(click.ParamType):
    name = "url"

    def convert(self, value, param, ctx):
        return URL(value)
