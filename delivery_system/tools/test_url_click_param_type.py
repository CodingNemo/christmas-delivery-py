from yarl import URL

from delivery_system.tools import URL_CLICK_TYPE


def test_should_convert_string_to_URL():
    url = "http://localhost:9090"

    converted_url = URL_CLICK_TYPE.convert(url, None, None)

    assert URL(url) == converted_url
