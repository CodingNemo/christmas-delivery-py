import backoff
from aiohttp import ClientConnectionError, ClientResponseError
from yarl import URL

from delivery_system.tools.url_click_param_type import UrlClickParamType

LOCALHOST = URL.build(scheme="http", host="localhost", port=80)
URL_CLICK_TYPE = UrlClickParamType()


def expo_backoff_on_http_client_exceptions(
    base=2, factor=1, max_value=None, max_tries=None
):
    return backoff.on_exception(
        backoff.expo,
        (ClientConnectionError, ClientResponseError),
        max_tries=max_tries,
        base=base,
        factor=factor,
        max_value=max_value,
    )


def as_dict(**params):
    return params
