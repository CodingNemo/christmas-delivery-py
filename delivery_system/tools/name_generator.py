import random
from threading import Lock


class NameGenerator:
    def __init__(self, possible_names) -> None:
        self.possible_names = possible_names
        self.name_occurences = {}
        self.sync = Lock()

    def new_name(self):
        name = self.__pick_name()

        with self.sync:
            occurence = self.__get_name_occurences(name)
            self.__increase_name_occurence(name)

            if occurence == 0:
                return name
            return name + " #" + str(occurence)

    def __increase_name_occurence(self, name):
        if name not in self.name_occurences.keys():
            self.name_occurences[name] = 1
        else:
            self.name_occurences[name] = self.name_occurences[name] + 1

    def __get_name_occurences(self, name):
        return self.name_occurences.get(name, 0)

    def __pick_name(self):
        name = random.choice(self.possible_names)
        return name
