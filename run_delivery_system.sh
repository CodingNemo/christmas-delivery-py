#!/usr/bin/env bash

SLEIGH_URL='http://localhost:8081'
MISS_CLAUS_URL='http://localhost:8080'

pipenv run sleigh --url ${SLEIGH_URL} &
sleep 5
pipenv run miss_claus --url ${MISS_CLAUS_URL} &
sleep 5
pipenv run elf --url http://localhost:9090 --miss-claus-url ${MISS_CLAUS_URL} --sleigh-url ${SLEIGH_URL} &
sleep 5
pipenv run factory --name "North Pole Factory" --miss-claus-url ${MISS_CLAUS_URL} &
sleep 5

